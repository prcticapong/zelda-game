#ifndef Entity_H
#define Entity_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
//#include "Score.h"

class Entity {

public:
	Entity();
	Entity(int x, int y, int w, int h);
	~Entity();

	virtual void init();
	virtual void update();
	virtual void render();


	//Setters and Getters
	void setRect(C_Rectangle a_rect);
	void setXY(int x, int y);

	void setX(int x);
	void setY(int y);
	void setW(int w);
	void setH(int h);

	C_Rectangle getRect();
	int getX();
	int getY();
	int getW();
	int getH();


	void setSpeed(int speed);
	int getSpeed();
	void setColor(ofColor color);

	void setCollisionMap(std::vector<std::vector<bool>>* colmap);



protected:
	void updateGraphic();

	virtual void updateControls();
	bool checkCollisionWithMap();

	void move();

	C_Rectangle		mpRect;
	C_Rectangle		mpGraphicRect;
	ofImage*		mpGraphicImg;


	int				mFrame;
	int				mCurrentFrameTime;

	int				mpDirection;	//Direcci�n
	int				mpSpeed;		//Velocidad
	bool			mpMoving;

	int				mpXtoGo;
	int				mpYtoGo;
	bool			mpAlive;

	ofColor			mpColor;	//Color

	std::vector<std::vector<bool>>* mpColMap;

};

#endif