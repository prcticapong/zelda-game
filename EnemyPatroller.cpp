#include "singletons.h"

//Include our classes
#include "Scene.h"
#include "EnemyPatroller.h"
#include "Entity.h"
#include "includes.h"

EnemyPatroller::EnemyPatroller() {

	mpGraphicIDEnemyPatroller = sResManager->getGraphicID("patroller.png");

}

EnemyPatroller::EnemyPatroller(int x, int y, int w, int h) {
	
};


EnemyPatroller::~EnemyPatroller() {

}


void EnemyPatroller::init() {

}

void EnemyPatroller::update() {
	Enemy::update();
	return;
}

bool EnemyPatroller::getAlive() {
	return mpAlive;
}

void EnemyPatroller::setAlive(bool alive) {
	mpAlive = alive;
}

void EnemyPatroller::updateControls() {
	Enemy::updateControls();
	return;
}
	
void EnemyPatroller::render() {
	imgRender(mpGraphicIDEnemyPatroller, mpRect.x, mpRect.y, mpGraphicRect, 255);
	return;

}

void EnemyPatroller::updateGraphic() {

}