#include "singletons.h"

//Include our classes
#include "Scene.h"
#include "EnemyChaser.h"
#include "Entity.h"
#include "includes.h"

EnemyChaser::EnemyChaser() {

	mpGraphicIDEnemyChaser = sResManager->getGraphicID("enemychaser.png");
	mpSpeed = 100;

}

EnemyChaser::EnemyChaser(int x, int y, int w, int h) {

};


EnemyChaser::~EnemyChaser() {

}


void EnemyChaser::init() {

}

void EnemyChaser::update() {
	Enemy::update();

	return;
}

bool EnemyChaser::getAlive() {
	return mpAlive;
}

void EnemyChaser::setAlive(bool alive) {
	mpAlive = alive;
}

void EnemyChaser::updateControls() {
	Enemy::updateControls();

	return;
}

void EnemyChaser::render() {
	imgRender(mpGraphicIDEnemyChaser, mpRect.x, mpRect.y, mpGraphicRect, 255);

	return;
}

void EnemyChaser::updateGraphic() {

}