#ifndef EnemyShooter_H
#define EnemyShooter_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
#include "Entity.h"
#include "Enemy.h"

class EnemyShooter:public Enemy {
	 
public:
	
	EnemyShooter();
	EnemyShooter(int x, int y, int w, int h);
	~EnemyShooter();

	void init();
	void update();
	void render();
	void updateControls();
	bool getAlive();
	void setAlive(bool alive);


protected:

	void        updateGraphic();
	int			mpGraphicIDEnemyShooter;
	bool		mpAlive;

};

#endif