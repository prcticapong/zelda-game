#ifndef EnemyPatroller_H
#define EnemyPatroller_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
#include "Entity.h"
#include "Enemy.h"

class EnemyPatroller:public Enemy {
	 
public:
	
	EnemyPatroller();
	EnemyPatroller(int x, int y, int w, int h);
	~EnemyPatroller();

	void init();
	void update();
	void render();
	void updateControls();
	bool getAlive();
	void setAlive(bool alive);


protected:

	void        updateGraphic();
	int			mpGraphicIDEnemyPatroller;
	bool		mpAlive;

};

#endif