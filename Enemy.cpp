#include "singletons.h"

//Include our classes
#include "Scene.h"
#include "Enemy.h"

Enemy::Enemy() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;
	mpSpeed = 150;
	mpColor = ofColor(0, 0, 0);
	mpDirection = NONE;
	mpMoving = false;
	mpXtoGo = 0;
	mpYtoGo = 0;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mFrame = 0;
	mCurrentFrameTime = 0;
	
}

Enemy::Enemy(int x, int y, int w, int h) {
	Enemy();
	setXY(x, y);
	setW(w);
	setH(h);
	mpXtoGo = x;
	mpYtoGo = y;
}


Enemy::~Enemy() {

}


void Enemy::init() {

}



void Enemy::update() {
	updateControls();
	if (!checkCollisionWithMap()) {		//checkCollisionWithMap() == false
		move();
	}else {
		//updateControls();
		mpMoving = false;
	}
	return;
}

bool Enemy::getAlive() {
	return mpAlive;
}

void Enemy::setAlive(bool alive) {
	mpAlive = alive;
}


void Enemy::updateControls() {
if (!mpMoving) {
	int move_enemy = rand() % 4;
		if (move_enemy == 0) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y - TILE_SIZE;
			mpMoving = true;
		}else
		if (move_enemy == 1) {
			mpXtoGo = mpRect.x - TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
		}else
		if (move_enemy == 2) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y + TILE_SIZE;
			mpMoving = true;
		}else
		if (move_enemy == 3) {
			mpXtoGo = mpRect.x + TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
		}
	}

	return;
}

void Enemy::render() {

	imgRender(mpGraphicIDEnemy, mpRect.x, mpRect.y, mpGraphicRect, 255);
	return;
}

void Enemy::updateGraphic() {

}







