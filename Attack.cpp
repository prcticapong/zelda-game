#include "singletons.h"

//Include our classes

#include "Attack.h"
#include "Entity.h"

Attack::Attack() :Entity() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;
	mpSpeed = 150;
	mpMoving = false;
	mpAlive = false;
	mpWeapon = false;
	mpLoad = false;
	mpGraphicImg = new ofImage();
	mpCount = 0;
}

Attack::Attack(int x, int y, int w, int h) : Attack() {
	setXY(x, y);
	setW(w);
	setH(h);
}

Attack::~Attack() {

}

void Attack::init() {

}

void Attack::update() {
	if (!mpAlive) {
		return;
	}
	mpCount += global_delta_time;
	if (mpCount > 1000) {
		mpCount = 0;
		mpAlive = false;
		return;
	}
	if (mpLoad) {
		mpSpeed = 150;
		updateControls();
		if (!checkCollisionWithMap()) {
			moving();
		}
		else {
			mpMoving = false;
			mpAlive = false;
		}
	}
	return;
}

void Attack::render()
{
}

bool Attack::isOfClass(std::string classType)
{
	return false;
}

void Attack::setWeapon(bool attack)
{
}

void Attack::setLoad(bool attack)
{
}

bool Attack::getWeapon()
{
	return false;
}

bool Attack::setLoad()
{
	return false;
}

void Attack::updateControls() {
	if (mpMoving) {
		return;
	}
	switch (mpDirection) {
	case UP:
		mpYtoGo = mpRect.y - TILE_SIZE;
		mpXtoGo = mpRect.x;
		mpMoving = true;
		break;

	case DOWN:
		mpYtoGo = mpRect.y + TILE_SIZE;
		mpXtoGo = mpRect.x;
		mpMoving = true;
		break;

	case LEFT:
		mpXtoGo = mpRect.x - TILE_SIZE;
		mpYtoGo = mpRect.y;
		mpMoving = true;
		break;

	case RIGHT:
		mpXtoGo = mpRect.x + TILE_SIZE;
		mpYtoGo = mpRect.y;
		mpMoving = true;
		break;
	default:
		break;
	}

}

void Attack::moving() {
	if (!mpMoving) {
		return;
	}
	int xAux = mpRect.x;
	int yAux = mpRect.y;
	if (mpRect.x < mpXtoGo) {
		mpRect.x += mpSpeed * global_delta_time / 1000;
	}
	else if (mpRect.x > mpXtoGo) {
		mpRect.x -= mpSpeed * global_delta_time / 1000;
	}
	if (mpRect.y < mpYtoGo) {
		mpRect.y += mpSpeed * global_delta_time / 1000;
	}
	else if (mpRect.y > mpYtoGo) {
		mpRect.y -= mpSpeed * global_delta_time / 1000;
	}
	if ((xAux < mpXtoGo && mpRect.x > mpXtoGo) || (xAux > mpXtoGo && mpRect.x < mpXtoGo)) {
		mpRect.x = mpXtoGo;
	}
	if ((yAux < mpYtoGo && mpRect.y > mpYtoGo) || (yAux > mpYtoGo && mpRect.y < mpYtoGo)) {
		mpRect.y = mpYtoGo;
	}

	if (mpRect.y == mpYtoGo && mpRect.x == mpXtoGo) {
		mpMoving = false;
	}

	return;
}