#ifndef EnemyChaser_H
#define EnemyChaser_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
#include "Entity.h"
#include "Enemy.h"

class EnemyChaser:public Enemy {
	 
public:
	
	EnemyChaser();
	EnemyChaser(int x, int y, int w, int h);
	~EnemyChaser();

	void init();
	void update();
	void render();
	void updateControls();
	bool getAlive();
	void setAlive(bool alive);


protected:

	void        updateGraphic();
	int			mpGraphicIDEnemyChaser;
	bool		mpAlive;

};

#endif