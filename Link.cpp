#include "singletons.h"

//Include our classes
#include "Scene.h"
#include "Link.h"
#include "includes.h"

Link::Link() : Entity() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;

	mpSpeed = 200;

	mpColor = ofColor(255, 255, 255);

	mpMoving = false;

	mpXtoGo = 0;
	mpYtoGo = 0;

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicIDLink = sResManager->getGraphicID("linku.png");

	mFrame = 0;
	mCurrentFrameTime = 0;
	
}

Link::Link(int x, int y, int w, int h) : Entity (x,y,w,h){
	Link();
	setXY(x, y);
	setW(w);
	setH(h);
	mpXtoGo = x;
	mpYtoGo = y;
}


Link::~Link() {

}


void Link::init() {

}


void Link::update() {
	updateControls();
	if (!checkCollisionWithMap()) {		//checkCollisionWithMap() == false
		move();
	}else {
		mpMoving = false;
	}
	updateGraphic();
	return;
}

void Link::updateGraphic() {

}

void Link::render() {
	imgRender(mpGraphicIDLink, mpRect.x, mpRect.y, mpGraphicRect, 255);
	return;
}

bool Link::checkCollisionWithMap() {
	//if (mpDirection == NONE) { return  false; }
	int checkX = mpXtoGo / TILE_SIZE;
	int checkY = mpYtoGo / TILE_SIZE;
	
	if (checkY < 0 || checkY >= (*mpColMap).size()) {
		return true;
	}

	if (checkX < 0) {
		mpRect.x = ((*mpColMap)[0].size() - 1)*TILE_SIZE;
		mpXtoGo = mpRect.x;
		return false;
	}
	if (checkX >= (*mpColMap)[0].size()) {
		mpRect.x = 0;
		mpXtoGo = mpRect.x;
		return false;
	}
	return (*mpColMap)[checkY][checkX];
}
//Bordes de pantalla

