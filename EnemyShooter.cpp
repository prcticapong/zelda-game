#include "singletons.h"

//Include our classes
#include "Scene.h"
#include "EnemyShooter.h"
#include "Entity.h"
#include "includes.h"

EnemyShooter::EnemyShooter() {

	mpGraphicIDEnemyShooter = sResManager->getGraphicID("shooter.png");

}

EnemyShooter::EnemyShooter(int x, int y, int w, int h) {
	
};


EnemyShooter::~EnemyShooter() {

}


void EnemyShooter::init() {

}

void EnemyShooter::update() {
	Enemy::update();
	return;
}

bool EnemyShooter::getAlive() {
	return mpAlive;
}

void EnemyShooter::setAlive(bool alive) {
	mpAlive = alive;
}

void EnemyShooter::updateControls() {
	Enemy::updateControls();
	return;
}
	
void EnemyShooter::render() {
	imgRender(mpGraphicIDEnemyShooter, mpRect.x, mpRect.y, mpGraphicRect, 255);
	return;

}

void EnemyShooter::updateGraphic() {

}