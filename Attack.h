#ifndef ATTACK_H
#define ATTACK_H

#include "includes.h"
#include "ofMain.h"
#include "Entity.h"

class Attack :public Entity {
public:

	Attack();
	Attack(int x, int y, int w, int h);
	~Attack();

	void init();
	void update();
	void render();

	bool isOfClass(std::string classType);
	std::string getClassName() {
		return "Attack";
	};

	//Setters and Getters
	void setWeapon(bool attack);
	void setLoad(bool attack);
	bool getWeapon();
	bool setLoad();

protected:

	void		updateControls();
	void		moving();
	int			mpCount;
	bool		mpWeapon;
	bool		mpLoad;

	//int			mpTimer;

};

#endif