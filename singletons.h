#ifndef SINGLETONS_H
#define SINGLETONS_H

#include "ResourceManager.h"
#include "SceneDirector.h"
//#include "GameState.h"

extern ResourceManager*	sResManager;	/*!<  Handler for loading graphical assets*/
extern SceneDirector*	sDirector;		/*!<  Handler for Scenes*/
//extern GameState*		sGameState;		/*!<  Handler for game variables and save/load */

void instanceSingletons();

#endif