#ifndef Enemy_H
#define Enemy_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
#include "Entity.h"


//enum ghostDirections { NONE, UP, DOWN, LEFT, RIGHT };

class Enemy:public Entity {

public:
	
	Enemy();
	Enemy(int x, int y, int w, int h);
	~Enemy();

	virtual void init();
	virtual void update();
	virtual void render();
	virtual void updateControls();
	bool getAlive();
	void setAlive(bool alive);


protected:
	void        updateGraphic();
	int			mpGraphicIDEnemy;
	bool		mpAlive;

};

#endif