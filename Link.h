#ifndef Link_H
#define Link_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"
#include "Entity.h"
//#include "Score.h"

class Link : public Entity {

public:
	Link();
	Link(int x, int y, int w, int h);
	~Link();

	void init();
	void update();
	void render();

protected:
	void		updateGraphic();
	bool		checkCollisionWithMap();
	int			mpGraphicIDLink;

};

#endif