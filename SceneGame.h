#ifndef SCENEGAME_H
#define SCENEGAME_H

#include "Scene.h"
#include "Link.h"
#include "Enemy.h"
#include "EnemyChaser.h"
#include "Attack.h"
//#include ""

//! SceneGame class
/*!
	Handles the Scene for the levels of the game.
*/
class SceneGame : public Scene
{
public:


	//! Constructor of an empty SceneMenu.
	SceneGame();

	//! Destructor
	~SceneGame();

	//! Initializes the Scene.
	virtual void init();

	//! Loads the scene (reinitializes)
	virtual void load();

	//! Returns the ClassName of the object
	/*!
		\return ClassName (as a string)
	*/
	virtual std::string getClassName() { return "SceneGame"; };

	//Player and Enemies
	Link* mpPlayer;
	Attack* mpAttack;
	std::vector<Enemy*> mpEnemy;
	std::vector<Enemy*> mpEnemyChaser;
	std::vector<Enemy*> mpEnemyShooter;
	std::vector<Enemy*> mpEnemyPatroller;

	//Render
	void renderMap();

protected:
	//! Updates the Scene
	void updateScene();

	//! Draws the Scene
	void drawScene();

	//! Takes keyboard input and performs actions
	virtual void inputEvent();

	int mpOffsetX;
	int mpOffsetY;

	C_Rectangle	mpGraphicRect;

	std::vector<std::vector<bool>> mpCollisionMap;
};

#endif