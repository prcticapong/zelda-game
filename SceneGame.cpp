#include "singletons.h"

//Include our classes
#include "SceneGame.h"
#include "Attack.h"
#include "Enemy.h"
#include "EnemyChaser.h"
#include "EnemyShooter.h"
#include "EnemyPatroller.h"

#define TILE_SIZE 32

SceneGame::SceneGame() : Scene() { //Calls constructor in class Scene
}

SceneGame::~SceneGame() {
}

void SceneGame::init() {
	Scene::init(); //Calls to the init method in class Scene

	mpOffsetX = 0;
	mpOffsetY = 0;
	mpPlayer = new Link();
}
void SceneGame::load() {
	Scene::load(); //Calls to the init method in class Scene

	std::fstream file;
	std::string line;
	int h, w;

	file.open("Map.txt", std::ios::in);
	if (!file.is_open()) {
		std::cout << "Error" << std::endl;
		system("pause");
		exit(0);
	}
	w = 30;
	h = 15;

	mpCollisionMap.resize(h);
	for (int i = 0; i < h; i++) {
		mpCollisionMap[i].resize(w);
		std::getline(file, line);
		for (int j = 0; j < w; j++) {
			char c = line[j];
			mpCollisionMap[i][j] = false;
			switch (c) {
			case '#':		//Wall
				mpCollisionMap[i][j] = true;
				break;

			case 'L':		//Link
				mpPlayer->setXY(j*TILE_SIZE, i*TILE_SIZE);
				mpPlayer->setCollisionMap(&mpCollisionMap);
				break;

			case 'S': {		//EnemyShooter
				EnemyShooter* aEnemyShooter = new EnemyShooter();
				aEnemyShooter->setXY(j*TILE_SIZE + (TILE_SIZE - aEnemyShooter->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aEnemyShooter->getH()) / 2);
				aEnemyShooter->setCollisionMap(&mpCollisionMap);
				mpEnemyShooter.push_back(aEnemyShooter);
			}
					  break;

			case 'C': {		//EnemyChaser
				EnemyChaser* aEnemyChaser = new EnemyChaser();
				aEnemyChaser->setXY(j*TILE_SIZE + (TILE_SIZE - aEnemyChaser->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aEnemyChaser->getH()) / 2);
				aEnemyChaser->setCollisionMap(&mpCollisionMap);
				mpEnemyChaser.push_back(aEnemyChaser);
			}
					  break;

			case 'P': {		//EnemyPatroller
				EnemyPatroller* aEnemyPatroller = new EnemyPatroller();
				aEnemyPatroller->setXY(j*TILE_SIZE + (TILE_SIZE - aEnemyPatroller->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aEnemyPatroller->getH()) / 2);
				aEnemyPatroller->setCollisionMap(&mpCollisionMap);
				mpEnemyPatroller.push_back(aEnemyPatroller);
			}
					  break;
			}
		}
	}
	file.close();

}

void SceneGame::updateScene() {
	inputEvent();
	mpPlayer->update();

	int size = 0;
	int size1 = 0;
	int size2 = 0;

	C_Rectangle playerRect = mpPlayer->getRect();

	size = mpEnemyChaser.size();
	for (int i = 0; i < size; i++) {
		Enemy* aEnemy = mpEnemyChaser[i];
		aEnemy->update();
		if (aEnemy->getAlive()) {
			if (C_RectangleCollision(playerRect, aEnemy->getRect())) {
				aEnemy->setAlive(false);
			}
		}
	}

	size1 = mpEnemyShooter.size();
	for (int i = 0; i < size1; i++) {
		Enemy* aEnemy = mpEnemyShooter[i];
		aEnemy->update();
		if (aEnemy->getAlive()) {
			if (C_RectangleCollision(playerRect, aEnemy->getRect())) {
				aEnemy->setAlive(false);
			}
		}
	}

	size2 = mpEnemyPatroller.size();
	for (int i = 0; i < size2; i++) {
		Enemy* aEnemy = mpEnemyPatroller[i];
		aEnemy->update();
		if (aEnemy->getAlive()) {
			if (C_RectangleCollision(playerRect, aEnemy->getRect())) {
				aEnemy->setAlive(false);
			}
		}
	}

}

void SceneGame::drawScene() {
	ofSetColor(0, 0, 255);

	renderMap();

	mpPlayer->render();

	int size = mpEnemyChaser.size();
	for (int i = 0; i < size; i++) {
		mpEnemyChaser[i]->render();
	}

	int size1 = mpEnemyShooter.size();
	for (int i = 0; i < size1; i++) {
		mpEnemyShooter[i]->render();
	}

	int size2 = mpEnemyPatroller.size();
	for (int i = 0; i < size2; i++) {
		mpEnemyPatroller[i]->render();
	}

}



//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneGame::inputEvent() {
	if (key_released['b']) {
		sDirector->goBack();
	}
}


void SceneGame::renderMap() {

	ofSetColor(27, 0, 253);
	int sizeV = mpCollisionMap.size();
	int sizeH = 0;
	if (sizeV > 0) {
		sizeH = mpCollisionMap[0].size();
	}
	else {
		return;
	}

	for (int j = 0; j < sizeV; j++) {
		for (int i = 0; i < sizeH; i++) {
			if (mpCollisionMap[j][i]) {
				ofDrawRectangle(i*TILE_SIZE + mpOffsetX,
					j*TILE_SIZE + mpOffsetY,
					TILE_SIZE, TILE_SIZE);
			}
		}
	}
}